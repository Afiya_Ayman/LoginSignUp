<?php
namespace App\User;
use App\Model\Database as DB;
use App\User\Message;
use App\User\Utility;
class User extends DB
{
    public $table="users";
    public $id;
    public $firstName;
    public $lastName;
    public $email;
    public $password;

    public function prepare($data="")
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if(array_key_exists('first_name',$data))
        {
            $this->firstName=$data['first_name'];
        }
        if(array_key_exists('last_name',$data))
        {
            $this->lastName=$data['last_name'];
        }
        if(array_key_exists('email',$data))
        {
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data))
        {
            $this->password=md5($data['password']);
        }
    }

    public function __construct()
    {
        parent::__construct();
    }

    public function store()
    {
        $query="INSERT INTO `users` (`first_name`, `last_name`, `email`, `password`) VALUES ('".$this->firstName."', '".$this->lastName."', '".$this->email."', '".$this->password."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            Message::message("Successfull!");
            Utility::redirect('../../index.php');
        }
    }
}