-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2016 at 09:59 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loginsignup`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Aaa', 'Bbb', 'ayman@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(2, 'aaaa', 'bbbb', 'abcd@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(3, 'Aaa', 'Bbb', 'ayman@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(4, 'AaaBBB', 'BbbAaaa', 'abcd@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(5, 'Afiya', 'Ayman', 'ayman@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'Aaa', 'Bbb', 'abcd@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(7, 'AaaBBB', 'Chy', 'abcd@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(8, 'Ayman', 'Afiya', 'abcd@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(9, 'Ayman', 'Hasan', 'xyz@yahoo.com', '0533b3773ae10a87205eba7e99bf97ce'),
(10, 'Ayman', 'Chy', 'xyz@yahoo.com', '286d5e78aebba5f8f91f4da4984f65fb'),
(11, 'Afiya', 'Ayman', 'aymanfia@yahoo.com', '0533b3773ae10a87205eba7e99bf97ce');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
