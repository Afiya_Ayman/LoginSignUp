<?php
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\User\Message;
use App\User\Utility;
session_start();

//Utility::dd($_POST);
$obj=new Auth();
$obj->prepare($_POST);
$status=$obj->is_registered();
//Utility::dd($status);

if($status)
{
    $_SESSION['user_email']=$_POST['email'];
    return Utility::redirect('../Welcome.php');
}
else
{
    Message::message("Wrong Username or Password");
    return Utility::redirect('../../index.php');
}
