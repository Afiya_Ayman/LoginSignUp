<?php
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\User\Message;
use App\User\Utility;
session_start();

$obj=new Auth();
$status=$obj->logout();
if($status) {
    Message::message("You are successfully logged-out");
    return Utility::redirect('../../index.php');
}
