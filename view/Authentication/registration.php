<?php
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\User\Message;
use App\User\Utility;
session_start();

//Utility::dd($_POST);
$obj=new Auth();
$obj->prepare($_POST);
$status=$obj->is_exist();

    if($status)
    {
        Message::message("Email is taken already");
        return Utility::redirect('../../index.php');
    }
    else
    {
        $obj=new User();
        $obj->prepare($_POST);
        $obj->store();
    }



